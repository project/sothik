<?php
function sothik_preprocess_page(&$variables){
     // Primary nav.
  $variables['primary_nav'] = FALSE;
  if ($variables['main_menu']) {
    // Load the tree.
    $tree = menu_tree_page_data(variable_get('menu_main_links_source', 'main-menu'));

    // Localize the tree.
    if ($i18n) {
      $tree = i18n_menu_localize_tree($tree);
    }

    // Build links.
    $variables['primary_nav'] = menu_tree_output($tree);

    // Provide default theme wrapper function.
    $variables['primary_nav']['#theme_wrappers'] = array('menu_tree__primary');
  }
}
/**
 * Changing checkbox markup.
 *
 * @see theme_checkbox().
 */
function sothik_menu_local_action($variables) {
  $link = $variables ['element']['#link'];
  $link_class = substr($link['href'], strrpos($link['href'], '/') + 1);

  $output = '<li class="nav-item">';
  if (isset($link ['href'])) {
    $output .= l($link ['title'], $link ['href'], array('attributes' => array('class' => 'nav-link mdi-action-'. $link_class)), isset($link ['localized_options']) ? $link ['localized_options'] : array());
  }
  elseif (!empty($link ['localized_options']['html'])) {
    $output .= $link ['title'];

  }
  else {
    $output .= check_plain($link ['title']);
  }
  $output .= "</li>\n";

  return $output;
}
?>
