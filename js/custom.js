(function($) {

  $(document).ready(function() {
    // This command is used to initialize some elements and make them work properly
    $.material.init();
    // This is to make page header readable and beautiful
    $("h1.page-header").addClass("alert alert-info text-uppercase text-center");
    $("h2.pane-title").addClass("alert alert-info text-uppercase text-center");
    $("footer.footer").addClass("alert alert-info text-center");
    });

})(jQuery);
