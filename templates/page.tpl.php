<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 *  <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
 *             <div class="navbar-collapse collapse">
 *              <nav role="navigation">
 *                 <?php if (!empty($primary_nav)): ?>
 *                   <?php print render($primary_nav); ?>
 *                 <?php endif; ?>
 *                <?php if (!empty($secondary_nav)): ?>
 *                   <?php print render($secondary_nav); ?>
 *                 <?php endif; ?>
 *                 <?php if (!empty($page['navigation'])): ?>
 *                   <?php print render($page['navigation']); ?>
 *                 <?php endif; ?>
 *               </nav>
 *             </div>
 *       <?php endif; ?>
 *  <?php $main_menu_tree = menu_tree_all_data('main-menu'); ?>
                <?php $main_menu_expanded = menu_tree_output($main_menu_tree);?>
                <?php print render($main_menu_expanded); ?>
 * 
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 * mubassir: 
 * changed pull-left with media-left
 * added $site_name instead of t('home')
 * @ingroup themeable
 */
?>
<header id="navbar" class="shadow-md bg-primary pm-auto border-bottom"><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <nav class="navbar navbar-expand-md text-white">
          <?php if ($logo): ?>
          <a class="navbar-brand text-white" href="<?php print $front_page; ?>" title="<?php print $site_name; ?>"> <img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" /></a>
           <?php endif; ?>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <ul class="navbar-nav">
                <li class="nav-item p-2">
                  <a class="nav-link text-white" href="<?php print $front_page; ?>">Home</a>
                </li>
                <li class="nav-item p-2">
                  <a class="nav-link text-white" href="/admission">Admission</a>
                </li>
                <li class="nav-item p-2">
                  <a class="nav-link text-white" href="/Question-Bank">Question Bank</a>
                </li>
               
            </ul>
    </nav>
</div>
</header>

<main role="main" class="flex-shrink-0 mx-auto py-auto">
    <div class="container">
        <div>
        <?php if (!empty($page['header'])):  ?>
          <header role="banner" id="page-header" class="navbar bg-secondary">
              <div class="container">
            <span class="mt-5 bg-info" <?php print render($page['header']); ?> </span>
            </div>
          </header> <!-- /#page-header -->
        <?php endif; ?>
          <div class="clearfix">
        
             <?php if (!empty($page['sidebar_first'])): ?>
                  <aside class="col-sm-3" role="complementary">
                        <div class="container">
                    <?php print render($page['sidebar_first']); ?>
                        </div>
                  </aside>  <!-- /#sidebar-first -->
                <?php endif; ?>
        
                <section class="bg-light">
                  <?php if (!empty($page['highlighted'])): ?>
                    <div class="highlighted"><?php print render($page['highlighted']); ?></div>
                  <?php endif; ?>
                  <?php if (!empty($breadcrumb)): ?> 
                  <div class="text-reset"> <?php print $breadcrumb; ?> </div>
                  <?php endif; ?>
                <div class="bg-white shadow-lg border-bottom mx-auto"> 
                 <a id="main-content"></a>
                  <?php print render($title_prefix); ?>
                  <?php if (!empty($title)): ?>
                    <h1 class="pm-2 mx-auto text-center"><?php print $title; ?></h1> 
                  <?php endif; ?>
                  <?php print render($title_suffix); ?>
                  </div>
                  <?php print $messages; ?>
                  <?php if (!empty($tabs)): ?>
                    <?php print render($tabs); ?>
                  <?php endif; ?>
                  <?php if (!empty($page['help'])): ?>
                    <?php print render($page['help']); ?>
                  <?php endif; ?>
                  <?php if (!empty($action_links)): ?>
                    <ul class="action-links fixed-action-btn" style="bottom: 45px; right: 24px;">
                      <a class="btn btn-fab btn-raised btn-material-orange">
                        <i class="mdi-content-add"></i>
                      </a>
                      <ul>
                        <?php $reverse_links = array_reverse($action_links); print render($reverse_links); ?>
                      </ul>
                    </ul>
                  <?php endif; ?>
                  <?php print render($page['content']); ?>
                </section>
        
                <?php if (!empty($page['sidebar_second'])): ?>
                  <aside class="col-sm-3" role="complementary">
                    <div class="container">
                    <?php print render($page['sidebar_second']); ?>
                    </div>
                  </aside>  <!-- /#sidebar-second -->
                <?php endif; ?>                 
            </div>
        </div>
    </div>
</main>
 <?php if (!empty($page['study'])): ?>
                  <div class="row">
                    <div class="col-md-12">
                    <div class="container">
                    <?php print render($page['study']); ?>
                    </div>
                    </div>
                  </div>  <!-- /#sidebar-first -->
                <?php endif; ?>

<footer class="footer bg-secondary  mt-auto py-3">
  <div class="container">
   <span class="text-light"> <?php print render($page['footer']); ?> </span>
  </div>
</footer>
